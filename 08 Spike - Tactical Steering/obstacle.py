'''

An obstacle for agents to hide from

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path

class Obstacle(object):

    def __init__(self, world=None):
        # keep a reference to the world object
        self.world = world
        self.pos = Vector2D(randrange(100, world.cx - 100), randrange(100, world.cy - 100))
        self.radius = 25.0

    def render(self, color=None):
        egi.green_pen()
        egi.circle(self.pos, self.radius)