class Node(object):

    def __init__(self, action, depth):
        self.action = action
        self.parent = None
        self.depth = depth

        self.action_eat = action[1]['Eat']
        self.action_sleep = action[1]['Sleep']
        self.eat = 0
        self.sleep = 0

        #self.print_node()

    def print_node(self):
        print("=======================")
        print(self.action)
        print("D:" + str(self.depth) + " E:" + str(self.eat) +" s:" + str(self.sleep))

    def set_values(self):
        if self.parent:
            self.eat = max(self.parent.eat + self.action_eat, 0)
            self.sleep = max(self.parent.sleep + self.action_sleep, 0)