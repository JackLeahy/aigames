
class ProductiveBot(object):

    def __init__(self):
        self.concurrent_fleets = 5
        self.fleet_min = 10
        self.fleet_remain = 0.75

    def update(self, gameinfo):

        if len(gameinfo.my_fleets) < self.concurrent_fleets:

            if gameinfo.my_planets and gameinfo.not_my_planets:
                # select most weakest/productive planet
                dest = self.get_most_productive(gameinfo)

                #select strongest fleet planet
                src = self.get_strongest_fleet(gameinfo)

                # launch new fleet if there's enough ships and dest isnt null
                if src.num_ships > self.fleet_min and dest != None:
                    gameinfo.planet_order(src, dest, int(src.num_ships * self.fleet_remain))

    def get_strongest_fleet(self, gameinfo):
        return max(gameinfo.my_planets.values(), key=lambda p: p.num_ships)

    def get_most_productive(self, gameinfo):
        strongest_fleet = self.get_strongest_fleet(gameinfo).num_ships * self.fleet_remain
        growth_rate = 0
        num_ships = 1000
        dest = None

        for p in gameinfo.not_my_planets.values():
            # if the growth rate is the highest of all planets
            if p.growth_rate > growth_rate:
                #if the planet is the weakest of the highest productivity planets
                if p.num_ships < num_ships:
                    # if the planet is capturable
                    if p.num_ships < strongest_fleet:
                        growth_rate = p.growth_rate
                        num_ships = p.num_ships
                        dest = p
        return dest
