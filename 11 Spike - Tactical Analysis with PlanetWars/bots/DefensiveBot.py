
class DefensiveBot(object):

    def __init__(self):
        self.concurrent_fleets = 3
        self.fleet_min = 25
        self.fleet_remain = 0.75
        self.scout_planets = []
        self.scout_planet_max = 5
        self.max_planets = 5

    def update(self, gameinfo):

        if len(gameinfo.my_fleets) < self.concurrent_fleets:

            if gameinfo.my_planets and gameinfo.not_my_planets:

                if len(self.scout_planets) < self.scout_planet_max:
                    #print("SCOUTING")
                    dest = self.scout(gameinfo)
                elif len(gameinfo.my_planets) < self.max_planets + len(self.scout_planets):
                    #print("PRODUCING")
                    dest = self.get_most_productive(gameinfo)
                else:
                    #print("BLOCKING")
                    dest = self.block_incoming_fleet(gameinfo)
                #select strongest fleet planet
                src = self.get_strongest_fleet(gameinfo)

                # launch new fleet if there's enough ships and dest isnt null
                if src.num_ships > self.fleet_min and dest != None:
                    gameinfo.planet_order(src, dest, int(src.num_ships * self.fleet_remain))



    def get_strongest_fleet(self, gameinfo):
        return max(gameinfo.my_planets.values(), key=lambda p: p.num_ships)

    def get_most_productive(self, gameinfo):
        strongest_fleet = self.get_strongest_fleet(gameinfo).num_ships * self.fleet_remain
        growth_rate = 0
        num_ships = 1000
        dest = None

        for p in gameinfo.not_my_planets.values():
            # if the growth rate is the highest of all planets
            if p.growth_rate > growth_rate:
                #if the planet is the weakest of the highest productivity planets
                if p.num_ships < num_ships:
                    # if the planet is capturable
                    if p.num_ships < strongest_fleet:
                        growth_rate = p.growth_rate
                        num_ships = p.num_ships
                        dest = p
        return dest

    def scout(self, gameinfo):
        num_ships = 1000
        dest = None

        for p in gameinfo.neutral_planets.values():
            if p.num_ships < num_ships:
                if dest not in self.scout_planets:
                    num_ships = p.num_ships
                    dest = p

        if dest != None and dest not in self.scout_planets:
            self.scout_planets.append(dest)

        return dest

    def block_incoming_fleet(self, gameinfo):
        fleet_size = 0
        fleet = None

        for f in gameinfo.enemy_fleets.values():
            if f.dest in gameinfo.my_planets:
                if f.num_ships > fleet_size:
                    fleet_size = f.num_ships
                    fleet = f
        if fleet != None:
            return fleet.dest
        else:
            return None