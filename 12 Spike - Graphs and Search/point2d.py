''' 2D Point

Created for COS3002 AI for Games by Clinton Woodward cwoodward@swin.edu.au
Please don't share without permission.

'''
from math import sqrt

class Point2D(object):

    __slots__ = ('x','y')

    def __init__(self, x=0.0, y=0.0):
        self.x = x
        self.y = y

    def copy(self):
        return Point2D(self.x, self.y)

    def normalise(self):
        ''' normalise self to a unit vector of length = 1.0 '''
        x = self.x
        y = self.y
        l = sqrt(x*x + y*y)
        try:
            self.x = x/l
            self.y = y/l
        except ZeroDivisionError:
            self.x = 0.
            self.y = 0.
        return self

    def length(self):
        ''' return the length of the vector '''
        x = self.x
        y = self.y
        return sqrt(x*x + y*y)

    def __str__(self):
        return '(%5.2f,%5.2f)' % (self.x, self.y)

    def __iadd__(self, rhs):  # +=
        self.x += rhs.x
        self.y += rhs.y
        return self

    def __isub__(self, rhs):  # -=
        self.x -= rhs.x
        self.y -= rhs.y
        return self

    def __imul__(self, rhs):  # *=
        self.x *= rhs
        self.y *= rhs
        return self

    def __itruediv__(self, rhs):  # /=
        self.x /= rhs
        self.y /= rhs
        return self

    def __add__(self, rhs):  # self + rhs
        return Point2D(self.x+rhs.x, self.y+rhs.y)

    def __sub__(self, rhs):  # self - rhs
        return Point2D(self.x-rhs.x, self.y-rhs.y)

    def __mul__(self, rhs):  # self * rhs (scalar)
        return Point2D(self.x*rhs, self.y*rhs)
    def __rmul__(self, lhs):  # lhs (scalar) * self
        return Point2D(self.x*lhs, self.y*lhs)

    def __truediv__(self, rhs):  # self / rhs (scalar)
        return Point2D(self.x/rhs, self.y/rhs)
    def __rtruediv__(self, lhs):  # lhs (scalar) / self
        return Point2D(lhs/self.x, lhs/self.y)