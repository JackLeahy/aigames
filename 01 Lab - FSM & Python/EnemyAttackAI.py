distance_from_player = 10
attack_range = 5

player_damage = 3
player_health = 10
player_strength = player_damage * player_health
enemy_damage = 2
enemy_health = 10
enemy_strength = enemy_damage * enemy_health

states = ['attacking','moving_toward','moving_away', 'idle']
current_state = 'idle'

value = 0

while player_health > 0 and enemy_health > 0 and value < 10:
    print('Distance From Player: %s' % distance_from_player)
    print('Player Health: %s' % player_health)
    print('Enemy Health: %s' % enemy_health)

    if current_state == 'idle':
        print("IDLE")
        if enemy_strength > player_strength:
            if distance_from_player > attack_range:
                current_state = 'moving_toward'
            elif distance_from_player <= attack_range:
                current_state = 'attacking'
        elif enemy_strength <= player_strength and distance_from_player < 15:
            current_state = 'moving_away'
    elif current_state == 'moving_toward':
        print("Moving TOWARDS Player")
        distance_from_player -= 1
        if distance_from_player < attack_range:
            current_state = 'attacking'
    elif current_state == 'moving_away':
        print("Moving AWAY FROM Player")
        distance_from_player += 1
        if distance_from_player >= 15:
            current_state = 'idle'
    elif current_state == 'attacking':
        print("ATTACKING Player")
        player_health -= enemy_damage
        enemy_health -= player_damage
    print('--------------------------------------------')
    value += 1

if enemy_health > 0:
    print('Enemy Wins!')
else:
    print('Player Wins!')