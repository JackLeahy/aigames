'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward cwoodward@swin.edu.au

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians, sqrt
from random import random, randrange, uniform
from projectile import Projectile
from path import Path

class Shooter(object):

    def __init__(self, world=None, scale=10.0, mass=1.0, mode='rifle'):
        # keep a reference to the world object
        self.world = world
        self.mode = mode
        # where am i and where am i going? random start pos
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.force = Vector2D()  # current steering force
        self.accel = Vector2D() # current acceleration due to force
        self.mass = mass

        # data for drawing this agent
        self.color = 'RED'

        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]

        self.projectiles = {
            'rifle':
                {
                    'speed': 300,
                    'rof': 1.0,
                    'accuracy': 0,
                    'color': 'YELLOW'
                },
            'rocket':
                {
                    'speed': 150,
                    'rof': 1.0,
                    'accuracy': 0,
                    'color': 'RED'
                },
            'pistol':
                {
                    'speed': 300,
                    'rof': 1.0,
                    'accuracy': 0.8,
                    'color': 'WHITE'
                },
            'grenade':
                {
                    'speed': 150,
                    'rof': 1.0,
                    'accuracy': 1.0,
                    'color': 'GREEN'
                }
        }

        self.shoot_cooldown = 1

    def update(self, delta):
        self.shoot_cooldown = max(self.shoot_cooldown - delta, 0)

        if self.shoot_cooldown == 0:
            self.shoot()

        self.heading = (self.get_target() - self.pos).normalise()
        self.side = self.heading.perp()

    def render(self, color=None):
        # draw the ship
        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos, self.heading, self.side, self.scale)
        # draw it!
        egi.closed_shape(pts)

    def shoot(self):
        self.shoot_cooldown = self.projectiles[self.mode]['rof']

        random_accuracy = Vector2D(randrange(-100, 100),randrange(-100, 100)) * self.projectiles[self.mode]['accuracy']

        velocity = ((self.get_target() - self.pos) - random_accuracy).normalise() * (self.projectiles[self.mode]['speed'] )

        projectile = Projectile(self.world, self.pos,  velocity, self.projectiles[self.mode]['color'])
        self.world.projectiles.append(projectile)

    def get_target(self):
        agent = self.world.agents[0]

        intercept_vector = self.calculate_interception_point(agent.pos, agent.vel, self.pos.copy(), self.projectiles[self.mode]['speed'])

        egi.set_pen_color(name=self.projectiles[self.mode]['color'])
        egi.cross(intercept_vector, 10)

        return intercept_vector


    def calculate_interception_point(self, target_pos, target_vel, shooter_pos, projectile_speed):
        origin = target_pos - shooter_pos

        h1 = (target_vel.x * target_vel.y) + (target_vel.y * target_vel.y) - (projectile_speed * projectile_speed)
        h2 = (origin.x * target_vel.x) + (origin.y * target_vel.y)

        if h1 == 0:
            t = -origin.length_sq() / (2 * h2)
        else:
            minus_half = -h2 / h1

            discriminant = (minus_half * minus_half) - (origin.length_sq() / h1)
            if discriminant < 0:
                print("OOPS")
                return None

            root = sqrt(discriminant)

            t1 = minus_half + root
            t2 = minus_half - root

            tMin = min(t1, t2)
            tMax = max(t1, t2)

            if tMin > 0:
                t = tMin
            else:
                t = tMax

            if t < 0:
                return None

            interception = Vector2D(target_pos.x + t * target_vel.x, target_pos.y + t * target_vel.y)

            return interception


    def find_intercept_vector(self, target_pos, target_vel, shooter_pos, proj_speed):
        # direction to the target
        dir_to_target = (target_pos - shooter_pos).normalise()
        #
        target_vel_orth = target_vel.dot(dir_to_target) * dir_to_target
        #
        target_vel_tang = target_vel - target_vel_orth
        #
        proj_vel_tang = target_vel_tang
        #
        proj_vel_speed = proj_vel_tang.length()

        # see if shot is possible
        if proj_vel_speed > proj_speed:
            print("IMPOSSIBLE SHOT")
            return Vector2D(0,0)
        else:
            proj_speed_orth = sqrt(proj_speed * proj_speed - proj_vel_speed * proj_vel_speed)
            proj_vel_orth = dir_to_target * proj_speed_orth

            return proj_vel_orth + proj_vel_tang
