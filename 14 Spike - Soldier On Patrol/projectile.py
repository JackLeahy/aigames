'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward cwoodward@swin.edu.au

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path
import copy

class Projectile(object):

    def __init__(self, world=None, pos=None, vel=None, color='LIGHT_BLUE'):

        self.world = world
        self.vel = vel
        self.pos = pos.copy()

        self.size = 5

        self.color = color

    def update(self, delta):
        # update position
        #self.pos = self.pos
        #print(self.vel)
        self.pos += self.vel * delta

    def render(self, color=None):
        egi.set_pen_color(name=self.color)
        egi.circle(self.pos, self.size)
