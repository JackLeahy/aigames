'''An agent with Seek, Flee, Arrive, Pursuit behaviours

Created for COS30002 AI for Games by Clinton Woodward cwoodward@swin.edu.au

'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians, sqrt
from random import random, randrange, uniform
from path import Path
from projectile import Projectile

class Agent_Soldier(object):

    def __init__(self, world=None, scale=10.0, mass=1.0, h_mode='patrol', l_mode='seek'):
        # keep a reference to the world object
        self.world = world

        # where am i and where am i going? random start pos
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.force = Vector2D()  # current steering force
        self.accel = Vector2D() # current acceleration due to force
        self.mass = mass

        # data for drawing this agent
        self.color = 'RED'

        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]
        ### path to follow?
        self.path = Path()
        self.randomize_path()
        self.waypoint_threshold = 75

        # limits?
        self.max_speed = 5 * scale
        self.max_force = 500.0

        # debug draw info?
        self.show_info = False

        self.h_mode = h_mode
        self.l_mode = l_mode
        self.sight_range = 300

        #SHOOT
        self.ammo_max = 2
        self.ammo = 1
        self.shoot_duration = 0.5
        self.shoot_time = 0
        self.projectile_speed = 300

        #RELOAD
        self.reload_duration = 2
        self.reload_time = 0

    def transition(self):
        if len(self.world.get_agents_in_area(self.pos, self.sight_range)) > 0:
            self.h_mode = "attack"
            if self.ammo > 0:
                self.l_mode = "shoot"
            else:
                self.l_mode = "reload"
        else:
            self.h_mode = "patrol"
            to_target = self.path.current_pt() - self.pos
            dist = max(to_target.length() - self.waypoint_threshold, 0)
            if dist > 50:
                self.l_mode = "seek"
            elif dist < 1:
                self.path.inc_current_pt()
                self.l_mode = "seek"
            else:
                self.l_mode = "arrive"

    def calculate(self, delta):
        h_mode = self.h_mode
        l_mode = self.l_mode
        #PATROL
        if h_mode == 'patrol':
            #SEEK
            if l_mode == "seek":
                force = self.seek(self.path.current_pt())
            #ARRIVE
            else:
                force = self.arrive(self.path.current_pt())
        #ATTACK
        else:
            #SHOOT
            if l_mode == "shoot":
                force = Vector2D(0, 0)
                self.shoot(delta)
            #RELOAD
            else:
                force = Vector2D(0, 0)
                self.reload(delta)
        self.force = force
        return force

    def update(self, delta):
        self.transition()
        force = self.calculate(delta)

        force.truncate(self.max_force)  # <-- new force limiting code
        # determin the new accelteration
        self.accel = force / self.mass  # not needed if mass = 1.0
        # new velocity
        self.vel += self.accel * delta
        # check for limits of new velocity
        self.vel.truncate(self.max_speed)
        # update position
        self.pos += self.vel * delta
        # update heading is non-zero velocity (moving)
        if self.h_mode == "patrol" and self.vel.length_sq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        elif self.h_mode == "attack":
            self.vel = Vector2D(0, 0)
            self.heading = (self.get_target() - self.pos).normalise()
            self.side = self.heading.perp()
        # treat world as continuous space - wrap new position if needed
        self.world.wrap_around(self.pos)

    def render(self, color=None):
        ''' Draw the triangle agent with color'''
        # draw the path if it exists and the mode is follow
        if self.show_info:
            egi.red_pen()
            pts = self.path.get_pts()
            egi.polyline(pts)
            egi.circle(self.pos, self.sight_range)

        # draw the ship
        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos,self.heading, self.side, self.scale)
        egi.closed_shape(pts)

        egi.text_at_pos(self.pos.x, self.pos.y + 20, self.l_mode)

    def seek(self, target_pos):
        ''' move towards target position '''
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)

    def arrive(self, target_pos):
        ''' this behaviour is similar to seek() but it attempts to arrive at
            the target position with a zero velocity'''
        decel_rate = 2.5
        to_target = target_pos - self.pos
        dist = to_target.length()
        if dist > 0:
            # calculate the speed required to reach the target given the
            # desired deceleration rate
            speed = dist / decel_rate
            # make sure the velocity does not exceed the max
            speed = min(speed, self.max_speed)
            # from here proceed just like Seek except we don't need to
            # normalize the to_target vector because we have already gone to the
            # trouble of calculating its length for dist.
            desired_vel = to_target * (speed / dist)
            return (desired_vel - self.vel)
        return Vector2D(0, 0)

    def shoot(self, delta):
        self.shoot_time = max(self.shoot_time - delta, 0)

        #if shoot cooldown is ready
        if self.shoot_time == 0:
            #get leading position infront of target
            leading_target = self.get_target()
            velocity = (leading_target - self.pos).normalise() * self.projectile_speed

            #create projectile in world
            projectile = Projectile(self.world, self.pos,  velocity, "YELLOW")
            self.world.projectiles.append(projectile)

            #reset shoot cooldown and remove 1 ammo
            self.shoot_time = self.shoot_duration
            self.ammo -= 1

        #if no ammo left, reload
        if self.ammo == 0:
            self.reload_time = self.reload_duration

    def reload(self, delta):
        self.reload_time = max(self.reload_time - delta, 0)

        if self.reload_time == 0:
            self.ammo = self.ammo_max

    def get_target(self):
        agent = self.get_closest_agent()
        if agent != None:
            intercept_vector = self.calculate_interception_point(agent.pos, agent.vel, self.pos.copy(), 300)
        else:
            intercept_vector = Vector2D(0,0)
        return intercept_vector

    def calculate_interception_point(self, target_pos, target_vel, shooter_pos, projectile_speed):
        origin = target_pos - shooter_pos

        h1 = (target_vel.x * target_vel.y) + (target_vel.y * target_vel.y) - (projectile_speed * projectile_speed)
        h2 = (origin.x * target_vel.x) + (origin.y * target_vel.y)

        if h1 == 0:
            t = -origin.length_sq() / (2 * h2)
        else:
            minus_half = -h2 / h1

            discriminant = (minus_half * minus_half) - (origin.length_sq() / h1)
            if discriminant < 0:
                print("OOPS")
                return None

            root = sqrt(discriminant)

            t1 = minus_half + root
            t2 = minus_half - root

            tMin = min(t1, t2)
            tMax = max(t1, t2)

            if tMin > 0:
                t = tMin
            else:
                t = tMax

            if t < 0:
                return None

            interception = Vector2D(target_pos.x + t * target_vel.x, target_pos.y + t * target_vel.y)

            return interception



    def randomize_path(self):
        cx = self.world.cx
        cy = self.world.cy
        margin = min(cx, cy) * (1/6)
        self.path.create_random_path(5, 0, 0, cx, cy, looped=True)

    def get_closest_agent(self):
        closest_position = Vector2D(0,0)
        closest_distance = 10000
        closest_agent = None

        for agent in self.world.get_agents_in_area(self.pos, self.sight_range):
            to_target = agent.pos - self.pos
            dist = to_target.length()
            if dist < closest_distance:
                closest_distance = dist
                closest_position = agent.pos
                closest_agent = agent
        return closest_agent



