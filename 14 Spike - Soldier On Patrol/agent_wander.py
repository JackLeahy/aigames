'''
Wandering agent for use with soldier
'''

from vector2d import Vector2D
from vector2d import Point2D
from graphics import egi, KEY
from math import sin, cos, radians
from random import random, randrange, uniform
from path import Path

class Agent_Wander(object):

    def __init__(self, world=None, scale=10.0, mass=1.0):
        # keep a reference to the world object
        self.world = world
        # where am i and where am i going? random start pos
        dir = radians(random()*360)
        self.pos = Vector2D(randrange(world.cx), randrange(world.cy))
        self.vel = Vector2D()
        self.heading = Vector2D(sin(dir), cos(dir))
        self.side = self.heading.perp()
        self.scale = Vector2D(scale, scale)  # easy scaling of agent size
        self.force = Vector2D()  # current steering force
        self.accel = Vector2D() # current acceleration due to force
        self.mass = mass

        # data for drawing this agent
        self.color = 'LIGHT_BLUE'

        self.vehicle_shape = [
            Point2D(-1.0,  0.6),
            Point2D( 1.0,  0.0),
            Point2D(-1.0, -0.6)
        ]

        ### wander details
        # NEW WANDER INFO
        self.wander_target = Vector2D(1, 0)
        self.wd_value = 1.0
        self.wander_dist = self.wd_value * scale
        self.wr_value = 0.5
        self.wander_radius = self.wr_value * scale
        self.wj_value = 5.0
        self.wander_jitter = self.wj_value * scale
        self.bRadius = scale

        #HEALTH
        self.health = 5

        # limits?
        self.max_speed = 7.5 * scale
        self.max_force = 500.0

        # debug draw info?
        self.show_info = False

    def update(self, delta):
        self.check_for_damage()
        ''' update vehicle position and orientation '''
        # calculate and set self.force to be applied
        #force = self.calculate()
        force = self.wander(delta)
        force.truncate(self.max_force)  # <-- new force limiting code
        # determin the new accelteration
        self.accel = force / self.mass  # not needed if mass = 1.0
        # new velocity
        self.vel += self.accel * delta
        # check for limits of new velocity
        self.vel.truncate(self.max_speed)
        # update position
        self.pos += self.vel * delta
        # update heading is non-zero velocity (moving)
        if self.vel.length_sq() > 0.00000001:
            self.heading = self.vel.get_normalised()
            self.side = self.heading.perp()
        # treat world as continuous space - wrap new position if needed
        self.world.wrap_around(self.pos)

    def render(self, color=None):

        # draw the ship
        egi.set_pen_color(name=self.color)
        pts = self.world.transform_points(self.vehicle_shape, self.pos, self.heading, self.side, self.scale)
        egi.closed_shape(pts)
        egi.text_at_pos(self.pos.x, self.pos.y + 20, str(self.health))
        # draw wander info?
        if self.show_info:
            # calculate the center of the wander circle in front of the agent
            wnd_pos = Vector2D(self.wander_dist, 0)
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
            # draw the wander circle
            egi.green_pen()
            egi.circle(wld_pos, self.wander_radius)
            # draw the wander target (little circle on the big circle)
            egi.red_pen()
            wnd_pos = (self.wander_target + Vector2D(self.wander_dist, 0))
            wld_pos = self.world.transform_point(wnd_pos, self.pos, self.heading, self.side)
            egi.circle(wld_pos, 3)

    def speed(self):
        return self.vel.length()

    def seek(self, target_pos):
        ''' move towards target position '''
        desired_vel = (target_pos - self.pos).normalise() * self.max_speed
        return (desired_vel - self.vel)

    def wander(self, delta):
        ''' random wandering using a projected jitter circle '''
        wt = self.wander_target
        # this behaviour is dependent on the update rate, so this line must
        # be included when using time independent framerate.
        jitter_tts = self.wander_jitter * delta # this time slice
        # first, add a small random vector to the target's position
        wt += Vector2D(uniform(-1,1) * jitter_tts, uniform(-1,1) * jitter_tts)
        # re-project this new vector back on to a unit circle
        wt.normalise()
        # increase the length of the vector to the same as the radius
        # of the wander circle
        wt *= self.wander_radius
        # move the target into a position WanderDist in front of the agent
        target = wt + Vector2D(self.wander_dist, 0)
        # project the target into world space
        wld_target = self.world.transform_point(target, self.pos, self.heading, self.side)
        # and steer towards it
        return self.seek(wld_target)

    def refresh_wander_values(self):
        self.wander_dist = self.wd_value * 30.0
        self.wander_radius = self.wr_value * 30.0
        self.wander_jitter = self.wj_value * 30.0

    def check_for_damage(self):
        for projectile in self.world.projectiles:
            to_target = projectile.pos - self.pos
            dist = to_target.length()
            if dist < 5:
                self.health -= 1
                self.world.projectiles.remove(projectile)
        if self.health == 0:
            self.world.agents.remove(self)